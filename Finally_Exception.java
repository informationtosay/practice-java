package whileloop;

import java.io.File;
import java.io.IOException;

public class Finally_Exception {

	public static void main(String[] args) throws IOException {
		
		finallyDemo();
		throwsDemo();
		
	}

	private static void throwsDemo() throws IOException {//throws IOException compile time exception
            
		File file = new File("/home/ramu/Documents/resume.ramu.docx");
		file.createNewFile();
	}

	private static void finallyDemo() {
		try {
			int [] ar = {10,20,30};
			
			for(int i=0; i<10;i++) {
				System.out.println(ar[i]);
			}}catch(Exception a) {
				System.out.println("exp");
			}finally {
				System.out.println("final");

			}
		
	}

}
