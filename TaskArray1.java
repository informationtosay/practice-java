package whileloop;

public class TaskArray1 {

	public static void main(String[] args) {
		  int[] ar = {10, 20, 11, 10, 44, 20};

	        // Find the largest element in the array
	        int max = ar[0]; // Assume the first element is the largest

	        for (int i = 1; i < ar.length; i++) {
	            if (ar[i] > max) {
	                max = ar[i]; // Update max if current element is greater
	            }
	        }

	        System.out.println("The largest element in the array is: " + max);
	        
	        
	}

}
