package whileloop;

public class TaskArray2 {

	public static void main(String[] args) {
		 int[] ar1 = {10, 20, 30, 40, 50, 50, 40, 30, 20};
	        int[] ar2 = new int[ar1.length]; // Create ar2 with the same length as ar1

	        // Copy elements from ar1 to ar2
	        for (int i = 0; i < ar1.length; i++) {
	            ar2[i] = ar1[i];
	        }

	        // Print ar2 to verify the copy
	        System.out.println("Elements of ar2 (copied from ar1):");
	        for (int num : ar2) {
	            System.out.print(num + " ");
	}

}
	
}
