package whileloop;

public class ArrayObject 
{
	
	String name;
	int price;
	
	public ArrayObject(String name,int price)
	{
		this.name=name;
		this.price=price;
	}

	public static void main(String[] args) 
	{
		ArrayObject ao1 = new ArrayObject("soap",25);
		ArrayObject ao2 = new ArrayObject("oil",55);
		
		
		ArrayTwo at = new ArrayTwo();
		at.display(ao1);
		at.display(ao2);

         
	}

}
