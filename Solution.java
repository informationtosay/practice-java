package whileloop;




import java.util.Scanner;
import java.util.Stack;

public class Solution {
    public String makeGood(String s) {
        Stack<Character> stack = new Stack<>(); // Create a stack to store characters
        
        // Iterate through the input string
        for (int i = 0; i < s.length(); i++) {
            if (!stack.isEmpty() && Math.abs(stack.peek() - s.charAt(i)) == 32) {
                // If the stack is not empty and the current character and the top of the stack form a pair (opposite cases)
                stack.pop(); // Remove the top of the stack as it annihilates with the current character
            } else {
                stack.push(s.charAt(i)); // Otherwise, push the current character onto the stack
            }
        }
        
        // Construct the result string by popping characters from the stack
        StringBuilder sb = new StringBuilder();
        while (!stack.isEmpty()) {
            sb.append(stack.pop());
        }
        
        // Reverse the result string and convert it to a string
        return sb.reverse().toString();
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        Scanner scanner = new Scanner(System.in);

        String input1 = "leEeetcode";
        String output1 = solution.makeGood(input1);
        System.out.println(output1); // Expected output: "leetcode"

        String input2 = "abBAcC";
        String output2 = solution.makeGood(input2);
        System.out.println(output2); // Expected output: ""

        String input3 = "s";
        String output3 = solution.makeGood(input3);
        System.out.println(output3); // Expected output: "s"

        
        scanner.close();
    }
}


