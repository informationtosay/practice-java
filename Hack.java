package whileloop;

public class Hack {

	
		
		    public static int sqrt(int a) {
		        if (a == 0 || a == 1) {
		            return a;
		        }

		        int first = 1;
		        int last = a;
		        int ans = 0;

		        while (first <= last) {
		            int mid = first + (last - first) / 2;
		            if (mid <= a / mid) {
		            	first = mid + 1;
		                ans = mid;
		            } else {
		            	last = mid - 1;
		            }
		        }

		        return ans;
		    }

		    public static void main(String[] args) {
		        int no1 = 4;
		        int no2 = 8;
		        
		        System.out.println(sqrt(no1)); 
		        System.out.println(sqrt(no2)); 
		    }
		

	}


