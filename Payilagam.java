package whileloop;

public class Payilagam {

	public static void main(String[] args) {

		Team team =new Team();
		
		//1st private variable//
		
	String name1 = team.getName();   //get value and store
	       name1 ="RAMU";            // value assign this class only
	       System.out.println(name1);
	       
	       team.setName("Ramu C");   //value reassign
	       System.out.println(team.getName());
	       
			//2nd private variable//
	       
	      int age1 = team.getAge(); 
	          age1 = 30;
		       System.out.println(age1);
		       
		       team.setAge(40);
		       System.out.println(team.getAge());
		       
				//3rd private variable//
		       
		       String qul=team.getQualification();
		       System.out.println(qul);            //value not assign -null 
		       
		        team.setQualification("B.E");
		        System.out.println(team.getQualification());
		        
				//4th private variable//
		        
		        int mobileNo= team.getMobileNo();
		        
		        team.setMobileNo(753992042);
		        System.out.println(team.getMobileNo());

	}

}
