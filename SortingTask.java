package whileloop;

public class SortingTask {

	public static void main(String[] args) 
	{

		SortingTask sorting= new SortingTask ();
		
		sorting.task1Sorting();
		System.out.println();
		
		sorting.task_Descending_Sorting();
		System.out.println();
		
		sorting.task2Maximum();
		System.out.println();
		
		sorting.task3Min();
		System.out.println();
		
		sorting.task4Average();
		System.out.println();
		
		sorting.task5_2nd_MaximumNo();
		System.out.println();
		
		sorting.task5_2nd_MinimumNo();
		System.out.println();
	
	}
	
	public void task5_2nd_MinimumNo() 
	{
		 int[] ar = {7, 4, 9, 3, 1, 13};
	        
	        int min = ar[0];
	        int secondMin;
	        
	        if (ar.length > 1) {
	            secondMin = ar[1];
	        } else {
	            secondMin = ar[0];
	        }
	        
	        for (int i = 2; i < ar.length; i++) {
	            if (ar[i] < min) {
	                secondMin = min;
	                min = ar[i];
	            } else if (ar[i] < secondMin && ar[i] != min) {
	                secondMin = ar[i];
	            }
	        }
	        System.out.print("Second maximum number: " + secondMin);
    }
	
	public void task5_2nd_MaximumNo() 
	{
		 int[] ar = {7, 4, 9, 3, 1, 13};
	        
	        int max = ar[0];
	        int secondMax;
	        
	        if (ar.length > 1) {
	            secondMax = ar[1];
	        } else {
	            secondMax = ar[0];
	        }
	        
	        for (int i = 2; i < ar.length; i++) {
	            if (ar[i] > max) {
	                secondMax = max;
	                max = ar[i];
	            } else if (ar[i] > secondMax && ar[i] != max) {
	                secondMax = ar[i];
	            }
	        }
	        System.out.print("Second maximum number: " + secondMax);
    }

	
	public void task4Average() 
	{
		int [] ar = {7,4,9,3,1,13};

    int sum = 0;
    for (int i = 0; i < ar.length; i++) 
    {
        sum += ar[i];
    }
    
    int average = sum / ar.length;
    
    System.out.print("Average: " + average);
	}
	
	public void task3Min() 
	{
		int [] ar = {7,4,9,3,1,13};

		 int min = ar[0]; 
	        for (int i = 1; i < ar.length; i++) {
	            if (ar[i] < min) {
	                min = ar[i]; 
	            }
	        }

	        System.out.print("The lowest element in the array is: " + min);
		
	}

	public void task2Maximum() 
	{
		int [] ar = {7,4,9,3,1,13};

		 int max = ar[0]; 
	        for (int i = 1; i < ar.length; i++) {
	            if (ar[i] > max) {
	                max = ar[i]; 
	            }
	        }

	        System.out.print("The largest element in the array is: " + max);
		
	}
	
	public void task1Sorting() 
	{
		int [] sort = {7,4,9,3,1,13};
		
		for(int len=0;len<sort.length;len++) 
		{
		for(int i=0;i<sort.length-1;i++) 
		{
			if(sort[i]>sort[i+1]) 
			{
				int temp = sort[i];
				sort[i]=sort[i+1];
				sort[i+1]=temp;
			}
		}
			
	    }
		for(int j=0;j<sort.length;j++) 
		{
			System.out.print(sort[j]+" ");
		}
		
	}

	public void task_Descending_Sorting() 
	{
		int [] sort = {7,4,9,3,1,13};
		
		for(int len=0;len<sort.length;len++) 
		{
		for(int i=0;i<sort.length-1;i++) 
		{
			if(sort[i]<sort[i+1]) 
			{
				int temp = sort[i];
				sort[i]=sort[i+1];
				sort[i+1]=temp;
			}
		}
			
	    }
		for(int j=0;j<sort.length;j++) 
		{
			System.out.print(sort[j]+" ");
		}
		
	}

}
