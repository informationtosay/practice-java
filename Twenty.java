package whileloop;

public class Twenty {

	public static void main(String[] args) {
		Twenty ty=new Twenty();
		ty.strat();
	}

	private void strat() 
	{
		
           for(int col=1;col<=20;col++) 
           { 
        	   System.out.print(col+" ");

        	   if(col%5==0)
        	   {
                  System.out.println();
            }	   
        }
	}
}