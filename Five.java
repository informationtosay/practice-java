package whileloop;

public class Five {

	public static void main(String[] args) {
		Five fi=new Five();
		fi.decrement();
		fi.increment();

	}

	public void increment() 
	{
		for(int row=1;row<=5;row++) 
		{
		for(int col=1;col<=row;col++) 
		{
			System.out.print(col+" ");
		}
		System.out.println();
		}
	}

	public void decrement() {
		for(int row=5;row>=1;row--) 
		{
			
		for(int col=1;col<=row;col++) 
		{
			System.out.print(col+" ");
		}
		System.out.println();
	}
	}

}
