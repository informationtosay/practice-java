package whileloop;

public class Number {

	public static void main(String[] args) {

		Number no=new Number();
		no.task1(0);
		no.task2(10);

	}
	
	public void task2(int ten) 
	{
    	if(ten>=1) 
		{
			System.out.print(ten+" ");
			task2(ten-=2);

        }
		
	}
	
	public void task1(int three) 
	{
    	if(three<50) 
		{
			System.out.print(three+" ");

			task1(three+=3);
		}
	
	}

}
