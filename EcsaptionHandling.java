package whileloop;

import java.util.Scanner;

public class EcsaptionHandling {

	public static void main(String[] args) 
	{
		EcsaptionHandling eh = new EcsaptionHandling ();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no1: ");
		int no1 = sc.nextInt();
		System.out.println("Enter no2: ");
		int no2 = sc.nextInt();
		
		
		eh.sub(no1,no2);
		eh.add(no1,no2);
		eh.divide(no1,no2);

	}

	private void sub(int no1, int no2) {
			System.out.println("sub=>"+ (no1-no2));	
	}

	private void divide(int no1, int no2) {
		try {
		System.out.println("divide=>"+ (no1/no2));	
		
		int [] ar = new int[no1];// array not allowing (-) nagative numbers or value
		
		for(int i=0;i<10;i++) { 
			System.out.println(ar[i]);
		}
		}catch(ArrayIndexOutOfBoundsException obe) {
			//ArrayIndexOutOfBoundsException or Exception class use
			System.out.println("Final Exception");
			
		}catch(ArithmeticException ae){
			//ArithmeticException or Exception class use
			
			System.out.println("check no2");
		}catch(NegativeArraySizeException nae) {
			//NegativeArraySizeException or Exception class use
			
			System.out.println("Enter valid input");
		}
	}

	private void add(int no1, int no2) {
		System.out.println("add=>"+ (no1+no2));	
		
	}

}
