package whileloop;

public class StringMethods {

	public static void main(String[] args) {
		
		String name = "RaManA";
		String name2 = new String ("Ram");
		
		System.out.println(name.equals(name2));// equal method
		
		System.out.println(name==name2);// equal check memory 
		
		System.out.println(name.equalsIgnoreCase(name2));//UPPERCASE and LOWERCASE match true come
		
		int index = name.indexOf("a",2);// index number come
		System.out.println(index);
		
		System.out.println(name.isEmpty());// value empty ya erukkanum 
		
		String dob= String.join("-","15","06","1993");// join method 
		System.out.println(dob);
		
		System.out.println(name.lastIndexOf("a"));// last indexof method
		
		System.out.println(name.replace("a","o"));// replace method
		
		System.out.println(name.startsWith("r"));// startsWith method
		
		System.out.println(name.endsWith("na"));// endsWith method
		
		System.out.println(name.subSequence(1, 3));// subSequence method
		
		System.out.println(name.substring(2, 5));// substring method , pathiya eduthu varuthu
		
		char [] self =name.toCharArray();
		
		for(int i=0;i<self.length;i++) {
			System.out.println(self[i]);
		}
		
		System.out.println(name.toLowerCase());
		
		System.out.println(name.toUpperCase());
		
		String beforeTrim = "      Ramu Cittarasan        ";
		String AfterTrim = beforeTrim.trim();
		
		System.out.println(AfterTrim);
		
		
	}

}
