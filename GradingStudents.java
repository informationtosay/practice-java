package whileloop;


		import java.util.ArrayList;
		import java.util.List;

		public class GradingStudents {

		    public static List<Integer> gradingStudents(List<Integer> grades) {
		        List<Integer> roundedGrades = new ArrayList<>();
		        for (int grade : grades) {
		            if (grade < 38) {
		                roundedGrades.add(grade);
		            } else {
		                int nextMultipleOf5 = ((grade / 5) + 1) * 5;
		                if (nextMultipleOf5 - grade < 3) {
		                    roundedGrades.add(nextMultipleOf5);
		                } else {
		                    roundedGrades.add(grade);
		                }
		            }
		        }
		        return roundedGrades;
		    }

		    public static void main(String[] args) {
		        List<Integer> grades = List.of(73, 67, 38, 33);
		        List<Integer> roundedGrades = gradingStudents(grades);
		        for (int grade : roundedGrades) {
		            System.out.println(grade);
		        }
		    }
		

	}


