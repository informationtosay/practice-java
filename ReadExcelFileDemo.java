package whileloop;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ReadExcelFileDemo  
{  
public static void main(String args[])  
{  


	try {
        // Specify the path to your Excel file

        FileInputStream file = new FileInputStream(new File("/home/ramu/Downloads/Chennai batting.xlsx"));
        

        // Create a workbook instance for Excel file
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        
        // Get the first sheet from the workbook

        XSSFSheet sheet = workbook.getSheetAt(0);
        // Iterate through each row in the sheet

        for (Row row : sheet) {
            // Iterate through each cell in the row

            for (Cell cell : row) {
                // Based on cell type, read the value

                switch (cell.getCellType()) {
                    case STRING:

                        System.out.print(cell.getStringCellValue() + "\t\t");
                        break;

                    case NUMERIC:
                        System.out.print(cell.getNumericCellValue() + "\t\t");

                        break;
                    case BOOLEAN:

                        System.out.print(cell.getBooleanCellValue() + "\t\t");
                        break;

                    default:
                        System.out.print("Unknown type\t\t");

                }
            }

            System.out.println();

        }
        

        // Close the workbook and file
        workbook.close();

        file.close();
    } catch (IOException e) {

        e.printStackTrace();
    }

}


}

	    
	





