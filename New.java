package whileloop;

public class New {

	public static void main(String[] args) {
		New nw=new New();
		nw.learn();
		nw.learn2();
		
	}

	public void learn2() {
		int std1=10;
		int std2=20;
		
		String rank = std1>std2 ? "std1" : std2>std1 ? "std2" : "std1,std2";
		System.out.println(rank);
	}

	public void learn() {
          int time=20;
          String result;
          
          if(time<18) 
          {
        	  result="Good day";
        	  }
          else
          {
        	  result="Good evening";		  
          }
          System.out.println(result);
          
          String res= time<21 ? "Good day" :"Good evening";
          System.out.println(res);
        	  
	}

}
