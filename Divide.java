package whileloop;

public class Divide {

	public static void main(String[] args) {
		
		int d=0;
		while(d<=100){
			
			if(d%2==0 && d%3==0) {
				
				System.out.print(d+ " ");
               	
			}		
			d++;
		
		}
	}  
}
