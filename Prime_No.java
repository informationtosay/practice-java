package whileloop;

public class Prime_No {

	public static void main(String[] args) 
{
		
		Prime_No prime = new Prime_No();
		prime.primeNo1();
		System.out.println();
		prime.primeNo2();
				
}

	private void primeNo2() {
		
		int number = 100;
		int count = 0;
		for(int i=1;i<=number;i++) 
		{
			if(number%i==0)
			{
				count++;
			}
		}
		if(count==2) 
		{
			System.out.println("is a prime number->"+number);
		}
		else
		{
			System.out.println("not a prime number ->"+ number);
		}
		  	
	}

	private void primeNo1() {
		for(int i=1;i<=100;i++)  // which numbers are (1 to 100) prime numbers. 
		{
			int count =0;
			for(int j=1; j<=i; j++) {
				if(i%j==0) {
					count++;
				}
			}
			if(count==2) {
				System.out.print(i +" ");
			}	
	}	
  }
}

