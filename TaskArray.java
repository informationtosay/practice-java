package whileloop;

public class TaskArray 
{

	public static void main(String[] args) 
	{
		int [] price = {10,11,20,11,22,44,55,20,10,9,5,4,55,24};//repeat numbers 10,11,20,55
		
		for(int start=0;start<price.length;start++)
		{
			for(int end =start+1;end<price.length;end++)
			{
				if(price[start]==price[end]) 
				{
					System.out.println(price[end]);
				}
			}
		}

	}

}
